from django.core.validators import RegexValidator, EmailValidator
from django.db import models


class Account(models.Model):
    """Account model to store users purchase data"""
    # user_id field probably should be a foreignkey to user model
    user_id = models.IntegerField()
    name = models.CharField(max_length=150)
    phone_number = models.CharField(max_length=13, validators=[
        RegexValidator(regex=r'^\+?1?\d{9,15}$',
                       message="Phone number must be entered in the format: '+9899999999'.")])
    email = models.EmailField()
    address = models.TextField()
    purchase = models.CharField(max_length=150)
    purchase_id = models.IntegerField()
    date = models.DateTimeField()  # deafult is creation date

    class Meta:
        verbose_name = "Account"
        ordering = ['-date']

    def __str__(self):
        return '{},{},{}'.format(self.user_id, self.purchase_id, self.date)

    @classmethod
    def load(cls, pk):
        account = cls.objects.get(pk=pk)
        return account
