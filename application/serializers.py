from rest_framework import serializers

from application.models import Account


class AccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = (
            'id', 'user_id', 'name', 'phone_number', 'email', 'address',
            'purchase', 'purchase_id', 'date')
