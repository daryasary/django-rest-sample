from django.http import Http404
from rest_framework.authentication import BasicAuthentication
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated

from application.models import Account
from application.serializers import AccountSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status


class AccountList(APIView):
    """Get all accounts list, create or update"""

    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)

    def get(self, request, **kwargs):
        accounts = Account.objects.all()
        serializer = AccountSerializer(accounts, many=True)
        return Response(serializer.data)

    def post(self, request, **kwargs):
        """Submit a new purchase to account model"""
        serializer = AccountSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AccountDetail(APIView):
    """Retrieve, update or delete a account instance."""

    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def load_purchase(pk):
        try:
            return Account.load(pk)
        except Account.DoesNotExist:
            raise Http404

    def get(self, request, pk, **kwargs):
        """Load specific account data"""
        account = self.load_purchase(pk)
        account = AccountSerializer(account)
        return Response(account.data)

    def put(self, request, pk, **kwargs):
        """Update an exiting account purchase info"""
        account = self.load_purchase(pk)
        serializer = AccountSerializer(account, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, **kwargs):
        """Delete saved purchase from account model"""
        account = self.load_purchase(pk)
        account.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
