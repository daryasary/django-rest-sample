# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Account',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('user_id', models.IntegerField()),
                ('name', models.CharField(max_length=150)),
                ('phone_number', models.CharField(max_length=13, validators=[django.core.validators.RegexValidator(message="Phone number must be entered in the format: '+9899999999'.", regex='^\\+?1?\\d{9,15}$')])),
                ('email', models.EmailField(max_length=254)),
                ('address', models.TextField()),
                ('purchase', models.CharField(max_length=150)),
                ('purchase_id', models.IntegerField()),
                ('date', models.DateTimeField()),
            ],
            options={
                'verbose_name': 'Account',
                'ordering': ['-date'],
            },
        ),
    ]
