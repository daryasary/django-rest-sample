from django.contrib import admin

from application.models import Account


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    fieldset = ('user_id', 'name', 'phone_number', 'email', 'address',
                'purchase', 'purchase_id', 'date')
    list_display = ('user_id', 'name', 'phone_number', 'email', 'address',
                    'purchase', 'purchase_id', 'date')
