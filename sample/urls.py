from django.conf.urls import include, url
from django.contrib import admin

from application import views

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/', views.AccountList.as_view(), name='account_list'),
    url(r'^account/(?P<pk>[0-9]+)/$', views.AccountDetail.as_view(),
        name='account'),
]
